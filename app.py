from flask import Flask,render_template,request,redirect,url_for,make_response,flash
import json,os,uuid,sys
from werkzeug import secure_filename
from subprocess import Popen

UPLOAD_FOLDER = './uploads/'
ALLOWED_EXTENSIONS = set(['csv'])


app=Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
def allowed_file(filename):
	return '.' in filename and \
		   filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS

@app.route('/results')

def results():
	return render_template('results.html')

@app.route('/save',methods=['POST'])
def save():
	response=make_response(redirect(url_for('results')))
	orderSheet=request.files['orderSheet']
	prefSheet=request.files['prefSheet']
	MOQ=request.form['MOQ']
	email=request.form['mail']
	print(email)
	if orderSheet and prefSheet and allowed_file(orderSheet.filename) and allowed_file(prefSheet.filename):
		orderSheetName = secure_filename(orderSheet.filename)
		prefSheetName = secure_filename(prefSheet.filename)
		userID=str(uuid.uuid4())
		os.makedirs(app.config['UPLOAD_FOLDER']+userID)
		orderSheet.save(os.path.join(app.config['UPLOAD_FOLDER']+userID, orderSheetName))
		prefSheet.save(os.path.join(app.config['UPLOAD_FOLDER']+userID, prefSheetName))
		p=Popen(["./untitled.sh",MOQ,os.path.join(app.config['UPLOAD_FOLDER']+userID, orderSheetName),os.path.join(app.config['UPLOAD_FOLDER']+userID, prefSheetName),email])

	return response

@app.route('/form')

def form():
	return render_template("form.html")

@app.route('/results/<useruuid>')

def probablyAUUID(useruuid):
	return useruuid

port = int(os.environ.get("PORT", 5000))
app.run(debug=True,host='0.0.0.0',port=port)

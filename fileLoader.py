import csv
import numpy
import pandas as pd
import matplotlib.pyplot as plt
import sys
import json
import os.path
from collections import OrderedDict
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import COMMASPACE, formatdate
from email import encoders

r=csv.reader(open(sys.argv[4]))
fieldNames2=next(r)

r=csv.reader(open(sys.argv[3]))
fieldNames1=next(r)
wkspFldr = os.path.dirname(sys.argv[3])
monthToDateDict={'July':'7/31/2014','August':'8/31/2014','September':'9/30/2014','October':'10/31/2014','November':'11/30/2014'}

globalSatisfiedStyleCodes=[]
unsatTable = pd.DataFrame(columns=('SalesGrp','UnsatIndex'))


def loadOrders(filename):
	orderData=pd.read_csv(filename)
	return orderData

df=pd.read_csv(sys.argv[3])
initial=df.copy()
preferenceArray=pd.read_csv(sys.argv[4])
preferenceArray=preferenceArray.sort([sys.argv[1]],ascending=[0])
for styleCode in df['Stylecode'].unique():
	if df[(df['Stylecode']==styleCode)]['Quantity'].sum()<int(sys.argv[2]):
		df=df[(df['Stylecode']!=styleCode)]
df=df.reset_index(drop=True)
grouped=df.groupby(['Stylecode','Month'],sort=True)
styleCodeGroups=[]
currentStyleCode=""
for name,group in grouped:
	if currentStyleCode==name[0]:
		styleCodeGroups.append((name,group))
	else:
		#time to process
		currentSums=[(x[1])['Quantity'].sum() for x in styleCodeGroups]
		truesAndFalses=[(x[1])['Quantity'].sum()<int(sys.argv[2]) for x in styleCodeGroups]
		if len(currentSums)>0:
			max_index = currentSums.index(max(currentSums))
			sumOfFalses=sum(x for i,x in enumerate(currentSums) if x<int(sys.argv[2]))
			for i,x in enumerate(currentSums):
					if x<int(sys.argv[2]):
						currentSums[i]=0
			if sumOfFalses<int(sys.argv[2]):
				#add to highest
				currentSums[max_index]+=sumOfFalses
			else:
				max_index=currentSums.index(max(x for i,x in enumerate(currentSums) if x<int(sys.argv[2])))
				currentSums[max_index]=sumOfFalses
		for i,(name2,group2) in enumerate(styleCodeGroups):

			if currentSums[i]==0:
				for x in group2.index:
					df.set_value(x,'Month',styleCodeGroups[max_index][0][1])
		styleCodeGroups=[(name,group)]
		currentStyleCode=name[0]
currentSums=[(x[1])['Quantity'].sum() for x in styleCodeGroups]
truesAndFalses=[(x[1])['Quantity'].sum()<int(sys.argv[2]) for x in styleCodeGroups]
if len(currentSums)>0:
	max_index = currentSums.index(max(currentSums))
	sumOfFalses=sum(x for i,x in enumerate(currentSums) if x<int(sys.argv[2]))
	for i,x in enumerate(currentSums):
			if x<int(sys.argv[2]):
				currentSums[i]=0
	if sumOfFalses<int(sys.argv[2]):
		#add to highest
		currentSums[max_index]+=sumOfFalses
	else:
		max_index=currentSums.index(max(x for i,x in enumerate(currentSums) if x<int(sys.argv[2])))
		currentSums[max_index]=sumOfFalses
for i,(name2,group2) in enumerate(styleCodeGroups):

	if currentSums[i]==0:
		for x in group2.index:
			df.set_value(x,'Month',styleCodeGroups[max_index][0][1])
styleCodeGroups=[(name,group)]
currentStyleCode=name[0]
df=df.reset_index(drop=True)

print(df)
grouped=df.groupby(['Stylecode','Month'],sort=True)
for name,group in grouped:
	if group['Quantity'].sum()<int(sys.argv[2]):
		print("FUCKFUCKFUCKFUCKFUCK")
thisMonthsOrders=df[(df['Month']==monthToDateDict[sys.argv[1]])].drop_duplicates()
fulfilledOrders=pd.DataFrame(columns=fieldNames1)
lastMonthsOrders=pd.DataFrame(columns=fieldNames1)
leftoverOrders=pd.DataFrame(columns=fieldNames1)

if(os.path.isfile(wkspFldr+'/lastMonthsOrders.csv')):
	fulfilledOrders=pd.read_csv(wkspFldr+'/lastMonthsOrders.csv').drop_duplicates()
	fulfilledOrders=fulfilledOrders.set_index('ID',verify_integrity=True)
	lastMonthsOrders=fulfilledOrders.copy()

discrepancy=pd.DataFrame(columns=(preferenceArray['SalesGrp']))

def calculateDiscrepancy():
	discrepancy.loc[0]=[0 for n in preferenceArray['SalesGrp']]
	for index,row in preferenceArray.iterrows():
		global df
		global fulfilledOrders
		totalOrdersActual=df[(df['Distributor']==row['SalesGrp'])]
		# # totalOrdersActual=totalOrdersActual[decodeNonUTFVector(totalOrdersActual['Month'])==monthToDateDict[sys.argv[1]]]['Quantity'].sum()
		idealPercentage=row[sys.argv[1]]
		plannedQuantity=fulfilledOrders[fulfilledOrders['Distributor']==row['SalesGrp']]['Quantity'].sum()

		if totalOrdersActual['Quantity'].sum()!=0:
			discrepancy.loc[0][row['SalesGrp']]=plannedQuantity/(totalOrdersActual['Quantity'].sum()*idealPercentage)
		else:
			discrepancy.loc[0][row['SalesGrp']]=0
calculateDiscrepancy()


def satisfyTheUnsatisfied(SalesGrp):
	global globalSatisfiedStyleCodes
	global fulfilledOrders
	sumAtPresent=fulfilledOrders[(fulfilledOrders['Distributor']==SalesGrp)]['Quantity'].sum()
	compPerc=preferenceArray[preferenceArray['SalesGrp']==SalesGrp][sys.argv[1]].sum()
	idealSum=df[(df['Distributor']==SalesGrp)]['Quantity'].sum()*compPerc
	currentSellerOrders=df[(df['Distributor']==SalesGrp)]
	if currentSellerOrders.shape[0]!=0:
		currentSellerOrders=currentSellerOrders[(currentSellerOrders['Month']==monthToDateDict[sys.argv[1]])]
		currentSellerOrders=currentSellerOrders[currentSellerOrders['Stylecode'].isin(globalSatisfiedStyleCodes)==False].sort(columns=['Quantity'],ascending=[0])
		for index,row in currentSellerOrders.iterrows():
			if sumAtPresent>idealSum:
				break
			sumAtPresent+=row['Quantity']
			df2=thisMonthsOrders[thisMonthsOrders['Stylecode']==row['Stylecode']]
			# r = [i for i in df2.index.values.tolist() if i in fulfilledOrders.index.values.tolist()]
			fulfilledOrders=fulfilledOrders.append(df2,verify_integrity=False)
			globalSatisfiedStyleCodes.append(row['Stylecode'])




for index,MDP in preferenceArray.sort([sys.argv[1]],ascending=[0]).iterrows():
	satisfyTheUnsatisfied(MDP['SalesGrp'])
	calculateDiscrepancy()
calculateDiscrepancy()
print(discrepancy.transpose())
choppedSum=0
def chopOff():
	global fulfilledOrders,leftoverOrders,thisMonthsOrders,choppedSum
	fulfilledOrders=fulfilledOrders.drop_duplicates()
	for index,row in preferenceArray.iterrows():
		notThisMonths=df[df['Month']!=monthToDateDict[sys.argv[1]]]
		i=0
		for index2,row2 in fulfilledOrders[(fulfilledOrders['Distributor']==row['SalesGrp'])].iterrows():
			notThisMonths=notThisMonths[notThisMonths['Stylecode']==row2['Stylecode']]
			if row2['Month']==monthToDateDict[sys.argv[1]] and discrepancy[int(row2['Distributor'])].iloc[0]>1 and fulfilledOrders[(fulfilledOrders['Stylecode']==row2['Stylecode'])]['Quantity'].sum()-row2['Quantity']>int(sys.argv[2]) and row2['Quantity']+notThisMonths['Quantity'].sum()>int(sys.argv[2]):
				leftoverOrders=leftoverOrders.append(row2,verify_integrity=True)
				fulfilledOrders.set_value(index2,'Quantity',0)
				thisMonthsOrders.set_value(index2,'Quantity',0)
				fulfilledOrders=fulfilledOrders[(fulfilledOrders['Quantity']!=0)]
				thisMonthsOrders=thisMonthsOrders[(thisMonthsOrders['Quantity']!=0)]
				choppedSum+=row2['Quantity']
				calculateDiscrepancy()
			i+=1
chopOff()
print(discrepancy.transpose())
if not os.path.isdir(wkspFldr+'/Results'):
	os.makedirs(wkspFldr+'/Results')
thisMonthsOrders.to_csv(wkspFldr+'/thisMonthsOrders.csv')
fulfilledOrders=fulfilledOrders.drop_duplicates()
fulfilledOrders.to_csv(wkspFldr+'/Results/fulfilledOrders'+sys.argv[1]+'.csv')
leftoverOrders=leftoverOrders.append(thisMonthsOrders[(thisMonthsOrders.index.isin(fulfilledOrders.index)==False)].drop_duplicates(),verify_integrity=True)
leftoverOrders.to_csv(wkspFldr+'/lastMonthsOrders.csv',index_label='ID')
print(thisMonthsOrders['Quantity'].sum())
print(fulfilledOrders['Quantity'].sum())
print(leftoverOrders['Quantity'].sum())
print(lastMonthsOrders['Quantity'].sum()+thisMonthsOrders['Quantity'].sum()-leftoverOrders['Quantity'].sum()-fulfilledOrders['Quantity'].sum()+choppedSum)


print("<<<<DIAGNOSTICS BEGIN >>>>")
fulfilledAndLeftover=fulfilledOrders.index.values.tolist()+leftoverOrders.index.values.tolist()
thisAndLastMonths=thisMonthsOrders.index.values.tolist()+lastMonthsOrders.index.values.tolist()
print(len(fulfilledAndLeftover))
print(len(thisAndLastMonths))
b3 = [val for val in thisAndLastMonths if val not in fulfilledAndLeftover]
print(len(b3))
print(df.iloc[b3].sort(columns=['Stylecode']))
print(df.iloc[b3]['Quantity'].sum())
print("<<<<DIAGNOSTICS END>>>>")

MDPTable=pd.DataFrame(columns=['title']+fulfilledOrders['Distributor'].unique().tolist())
tableRows=[]
tableRows.append([sys.argv[1]+' Actual'])
tableRows.append([sys.argv[1]+' Ideal'])
tableRows.append([sys.argv[1]+' Planned'])
tableRows.append([sys.argv[1]+' FP'])


for MDP in fulfilledOrders['Distributor'].unique():
	currentSellerActual=initial[(initial['Distributor']==MDP)]
	currentSellerActual=currentSellerActual[(currentSellerActual['Month'])==monthToDateDict[sys.argv[1]]]['Quantity'].sum()
	currentSellerIdeal=df[(df['Distributor']==MDP)]['Quantity'].sum()*preferenceArray[preferenceArray['SalesGrp']==MDP][sys.argv[1]].sum()
	currentSellerPlanned=fulfilledOrders[(fulfilledOrders['Distributor']==MDP)]['Quantity'].sum()
	currentSellerFP=currentSellerPlanned/currentSellerActual
	tableRows[0].append(currentSellerActual)
	tableRows[1].append(currentSellerIdeal)
	tableRows[2].append(currentSellerPlanned)
	tableRows[3].append(currentSellerFP)

for x in range(0,4):
	MDPTable.loc[x]=tableRows[x]


if os.path.isfile(wkspFldr+'/MDPTable.csv'):
	with open(wkspFldr+'/MDPTable.csv', 'a') as f:
		MDPTable.to_csv(f, header=False)
else:
	MDPTable.to_csv(wkspFldr+'/MDPTable.csv')



StylecodeTable=pd.DataFrame(columns=['title']+fulfilledOrders['Stylecode'].unique().tolist())
tableRows=[]
tableRows.append([sys.argv[1]+' Demand'])
tableRows.append([sys.argv[1]+' Produced'])

for styleCode in fulfilledOrders['Stylecode'].unique():
	Demand=df[(df['Stylecode']==styleCode)]['Quantity'].sum()
	Produced=fulfilledOrders[(fulfilledOrders['Stylecode']==styleCode)]['Quantity'].sum()
	tableRows[0].append(Demand)
	tableRows[1].append(Produced)

for x in range(0,2):
	StylecodeTable.loc[x]=tableRows[x]

if os.path.isfile(wkspFldr+'/StylecodeTable.csv'):
	with open(wkspFldr+'/StylecodeTable.csv', 'a') as f:
		StylecodeTable.to_csv(f, header=False)
else:
	StylecodeTable.to_csv(wkspFldr+'/StylecodeTable.csv')

if sys.argv[1]!='November':
	sys.exit()
else:
	path, filename = os.path.split(wkspFldr)
	print(filename.split())
	print('zip' + ' -r ' + ' ' + filename.split()[0] +'.zip' + ' '+ wkspFldr+ '/')
	os.system('zip' + ' -r ' + ' ' + filename.split()[0] +'.zip' + ' '+ wkspFldr )
	def send_mail( send_from, send_to, subject, text, files=[], server="smtp.gmail.com", port=587, username='', password='', isTls=True):
	    msg = MIMEMultipart()
	    msg['From'] = send_from
	    msg['To'] = COMMASPACE.join(send_to)
	    msg['Date'] = formatdate(localtime = True)
	    msg['Subject'] = subject

	    msg.attach( MIMEText(text) )

	    for f in files:
	        part = MIMEBase('application', "octet-stream")
	        part.set_payload( open(f,"rb").read() )
	        encoders.encode_base64(part)
	        part.add_header('Content-Disposition', 'attachment; filename="{0}"'.format(os.path.basename(f)))
	        msg.attach(part)

	    smtp = smtplib.SMTP(server, port)
	    if isTls: smtp.starttls()
	    smtp.login('fashionmadura@gmail.com','efE-hLS-9Gw-vHz')
	    smtp.sendmail(send_from, send_to, msg.as_string())
	    smtp.quit()
	send_mail('x@example.com',[sys.argv[5]],'daskfnas','daksfjhkdsfjhkdfjshafkdsjfahk',files=[filename.split()[0] +'.zip'])

	os.system('rm '+filename.split()[0] +'.zip')
